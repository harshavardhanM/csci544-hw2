
import sys, re, os

def main():

    inFile = sys.argv[1]    #training file = train.pos

    modelFile = sys.argv[2]   # model file, ouput of megam

    
    inF = open(inFile, 'r')

    tempmegaIPF = open('tempmegaInput.txt', 'w')

           
    
    for eachLine in inF:
        
        scrape1 = re.sub(r'[#]+', 'HASH', eachLine)

        tokens = scrape1.split()


        x = len(tokens)

        if x == 1:
            print(tokens[0].split('/')[1], 'currW:'+tokens[0].split('/')[0], 'prevW:begSent', 'nextW:endSent', file=tempmegaIPF)

        else:

            print(tokens[0].split('/')[1], 'currW:'+tokens[0].split('/')[0], 'prevW:begSent', 'nextW:'+tokens[1].split('/')[0], file=tempmegaIPF)

            for i in range(1, (x-1)):

                print(tokens[i].split('/')[1], 'currW:'+tokens[i].split('/')[0], 'prevW:'+tokens[i-1].split('/')[0], 'nextW:'+tokens[i+1].split('/')[0], file=tempmegaIPF)
                i+=1

            print(tokens[x-1].split('/')[1], 'currW:'+tokens[x-1].split('/')[0], 'prevW:'+tokens[x-2].split('/')[0], 'nextW:endSent', file=tempmegaIPF)
        
    
    tempmegaIPF.close()
    inF.close()

      

    os.system('./megam -maxi 20 -nc -pa multitron tempmegaInput.txt > ' + modelFile)



if __name__ == "__main__":
    main()